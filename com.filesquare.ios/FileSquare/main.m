//
//  main.m
//  FileSquare
//
//  Created by Mert on 6/16/12.
//  Copyright (c) 2012 Mert Dumenci. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "MDAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([MDAppDelegate class]));
    }
}
