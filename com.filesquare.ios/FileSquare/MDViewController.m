//
//  MDViewController.m
//  FileSquare
//
//  Created by Mert on 6/16/12.
//  Copyright (c) 2012 Mert Dumenci. All rights reserved.
//

#import "MDViewController.h"

@interface MDViewController ()

@end

@implementation MDViewController
@synthesize selfImage;

- (void)viewDidLoad
{
    [super viewDidLoad];
	UITapGestureRecognizer *selfImageTapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tappedOnSelfImage:)];
    UIPanGestureRecognizer *selfImagePanRecognizer = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(pannedOnSelfImage:)];
    
    [selfImage addGestureRecognizer:selfImageTapRecognizer], [selfImage addGestureRecognizer:selfImagePanRecognizer];
}

- (void)tappedOnSelfImage:(id)sender {
    picker = [[UIImagePickerController alloc] init];
    
    picker.delegate = self;
    
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
        
    {
        
        picker.sourceType = UIImagePickerControllerSourceTypeCamera;
        
    } else
        
    {
        
        picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        
    }
    
    [self presentModalViewController:picker animated:YES];
    
}
}
- (void)pannedOnSelfImage:(id)sender {
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
}

@end
