//
//  MDViewController.h
//  FileSquare
//
//  Created by Mert on 6/16/12.
//  Copyright (c) 2012 Mert Dumenci. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MDViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIImageView *selfImage;

- (void)tappedOnSelfImage:(id)sender;
- (void)pannedOnSelfImage:(id)sender;
@end
