//
//  MDAppDelegate.h
//  FileSquare
//
//  Created by Mert on 6/16/12.
//  Copyright (c) 2012 Mert Dumenci. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MDAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
