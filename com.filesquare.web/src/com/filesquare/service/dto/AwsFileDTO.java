package com.filesquare.service.dto;

import java.io.InputStream;
import java.io.Serializable;

public class AwsFileDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private String key;
	private InputStream input;
	private String contentType;
	private Long contentLength;

	public AwsFileDTO() {
	}

	public AwsFileDTO(String key, InputStream input, String contentType, Long contentLength) {
		this.key = key;
		this.input = input;
		this.contentType = contentType;
		this.contentLength = contentLength;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public InputStream getInput() {
		return input;
	}

	public void setInput(InputStream input) {
		this.input = input;
	}

	public String getContentType() {
		return contentType;
	}

	public void setContentType(String contentType) {
		this.contentType = contentType;
	}

	public Long getContentLength() {
		return contentLength;
	}

	public void setContentLength(Long contentLength) {
		this.contentLength = contentLength;
	}
}