package com.filesquare.service;

import java.io.File;
import java.io.InputStream;
import java.util.List;

import com.amazonaws.services.s3.model.Bucket;
import com.amazonaws.services.s3.model.ObjectListing;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.PutObjectResult;
import com.amazonaws.services.s3.model.S3Object;
import com.filesquare.service.dto.AwsFileDTO;


public interface AmazonService {
	public Boolean isBucketExist(String bucketName) throws Exception;
	
	public Bucket createBucket(String bucketName) throws Exception;
	
	public List<Bucket> listAllBuckets() throws Exception;
	
	public String uploadFile(AwsFileDTO awsImageDTO) throws Exception;
	
	public PutObjectResult uploadFile(String key, File file) throws Exception;
	
	public PutObjectResult uploadFile(String bucketName, String key, File file) throws Exception;
	
	public PutObjectResult uploadFile(String key, InputStream input, String contentType, Long contentLength) throws Exception;
	
	public PutObjectResult uploadFile(String bucketName, String key, InputStream input, String contentType, Long contentLength) throws Exception;
	
	public PutObjectResult uploadFile(String key, InputStream input, ObjectMetadata metadata) throws Exception;
	
	public PutObjectResult uploadFile(String bucketName, String key, InputStream input, ObjectMetadata metadata) throws Exception;
	
	public S3Object downloadFile(String bucketName, String key) throws Exception;
	
	public ObjectListing listFiles(String bucketName) throws Exception;
	
	public ObjectListing listFiles(String bucketName, String prefix) throws Exception;
	
	public void deleteBucket(String bucketName) throws Exception;
	
	public void deleteFile(String key) throws Exception;
	
	public void deleteFile(String bucketName, String key) throws Exception;
}