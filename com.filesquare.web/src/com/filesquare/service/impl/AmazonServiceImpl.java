package com.filesquare.service.impl;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import org.springframework.stereotype.Component;

import com.amazonaws.auth.PropertiesCredentials;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.Bucket;
import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.amazonaws.services.s3.model.CreateBucketRequest;
import com.amazonaws.services.s3.model.DeleteBucketRequest;
import com.amazonaws.services.s3.model.DeleteObjectRequest;
import com.amazonaws.services.s3.model.GetObjectRequest;
import com.amazonaws.services.s3.model.ListBucketsRequest;
import com.amazonaws.services.s3.model.ListObjectsRequest;
import com.amazonaws.services.s3.model.ObjectListing;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.services.s3.model.PutObjectResult;
import com.amazonaws.services.s3.model.S3Object;
import com.filesquare.service.AmazonService;
import com.filesquare.service.constant.ServicePropertiesConstants;
import com.filesquare.service.dto.AwsFileDTO;
import com.filesquare.service.properties.ServiceProperties;

@Component
public class AmazonServiceImpl implements AmazonService {
	
	private AmazonS3 s3Service;

	public Boolean isBucketExist(String bucketName) throws Exception {
		List<Bucket> buckets = listAllBuckets();

		if (buckets == null) {
			return false;
		}

		for (Bucket bucket : buckets) {
			if (bucketName.equals(bucket.getName())) {
				return true;
			}
		}

		return false;
	}

	public Bucket createBucket(String bucketName) throws Exception {
		AmazonS3 s3 = getS3Service();

		try {
			CreateBucketRequest createBucketRequest = new CreateBucketRequest(bucketName);

			return s3.createBucket(createBucketRequest);
		} catch (Throwable e) {
			throw new Exception(e);
		}
	}

	public List<Bucket> listAllBuckets() throws Exception {
		AmazonS3 s3 = getS3Service();

		try {
			ListBucketsRequest listBucketsRequest = new ListBucketsRequest();

			return s3.listBuckets(listBucketsRequest);
		} catch (Throwable e) {
			throw new Exception(e);
		}
	}

	public String uploadFile(AwsFileDTO awsImageDTO) throws Exception {
		uploadFile(awsImageDTO.getKey(), awsImageDTO.getInput(), awsImageDTO.getContentType(), awsImageDTO.getContentLength());
		
		return getFilepath() + awsImageDTO.getKey();
	}
	
	public PutObjectResult uploadFile(String key, File file) throws Exception {
		String bucketName = getBucketName();

		return uploadFile(bucketName, key, file);
	}

	public PutObjectResult uploadFile(String bucketName, String key, File file) throws Exception {
		AmazonS3 s3 = getS3Service();

		try {
			PutObjectRequest putObjectRequest = new PutObjectRequest(bucketName, key, file);
			putObjectRequest.setCannedAcl(CannedAccessControlList.PublicRead);

			return s3.putObject(putObjectRequest);
		} catch (Throwable e) {
			throw new Exception(e);
		}
	}

	public PutObjectResult uploadFile(String key, InputStream input, String contentType, Long contentLength) throws Exception {
		String bucketName = getBucketName();

		return uploadFile(bucketName, key, input, contentType, contentLength);
	}

	public PutObjectResult uploadFile(String bucketName, String key, InputStream input, String contentType, Long contentLength) throws Exception {
		try {
			ObjectMetadata metadata = new ObjectMetadata();
			metadata.setContentType(contentType);
			metadata.setContentLength(contentLength);

			return uploadFile(bucketName, key, input, metadata);
		} catch (Throwable e) {
			throw new Exception(e);
		}
	}

	public PutObjectResult uploadFile(String key, InputStream input, ObjectMetadata metadata) throws Exception {
		String bucketName = getBucketName();

		return uploadFile(bucketName, key, input, metadata);
	}
	
	public PutObjectResult uploadFile(String bucketName, String key, InputStream input, ObjectMetadata metadata) throws Exception {
		AmazonS3 s3 = getS3Service();

		try {
			PutObjectRequest putObjectRequest = new PutObjectRequest(bucketName, key, input, metadata);
			putObjectRequest.setCannedAcl(CannedAccessControlList.PublicRead);

			return s3.putObject(putObjectRequest);
		} catch (Throwable e) {
			throw new Exception(e);
		}
	}

	public S3Object downloadFile(String bucketName, String key) throws Exception {
		AmazonS3 s3 = getS3Service();

		try {
			GetObjectRequest getObjectRequest = new GetObjectRequest(bucketName, key);

			return s3.getObject(getObjectRequest);
		} catch (Throwable e) {
			throw new Exception(e);
		}
	}

	public ObjectListing listFiles(String bucketName) throws Exception {
		AmazonS3 s3 = getS3Service();

		try {
			ListObjectsRequest listObjectsRequest = new ListObjectsRequest().withBucketName(bucketName);

			return s3.listObjects(listObjectsRequest);
		} catch (Throwable e) {
			throw new Exception(e);
		}
	}

	public ObjectListing listFiles(String bucketName, String prefix) throws Exception {
		AmazonS3 s3 = getS3Service();

		try {
			ListObjectsRequest listObjectsRequest = new ListObjectsRequest().withBucketName(bucketName).withPrefix(prefix);

			return s3.listObjects(listObjectsRequest);
		} catch (Throwable e) {
			throw new Exception(e);
		}
	}

	public void deleteBucket(String bucketName) throws Exception {
		AmazonS3 s3 = getS3Service();

		try {
			DeleteBucketRequest deleteBucketRequest = new DeleteBucketRequest(bucketName);
			s3.deleteBucket(deleteBucketRequest);
		} catch (Throwable e) {
			throw new Exception(e);
		}
	}

	public void deleteFile(String key) throws Exception {
		String bucketName = getBucketName();
		deleteFile(bucketName, key);
	}

	public void deleteFile(String bucketName, String key) throws Exception {
		AmazonS3 s3 = getS3Service();

		try {
			DeleteObjectRequest deleteObjectRequest = new DeleteObjectRequest(bucketName, key);
			s3.deleteObject(deleteObjectRequest);
		} catch (Throwable e) {
			throw new Exception(e);
		}
	}

	private AmazonS3 getS3Service() throws Exception {
		if (s3Service == null) {
			try {
				PropertiesCredentials propertiesCredentials = 
					new PropertiesCredentials(ServiceProperties.class.getResourceAsStream(ServicePropertiesConstants.AWS_CREDENTIALS_PROPERTIES));
				s3Service = new AmazonS3Client(propertiesCredentials);
			} catch (IOException e) {
				throw new Exception(e);
			}
		}

		return s3Service;
	}

	private String getBucketName() throws Exception {
		try {
			return ServiceProperties.getMessage(ServicePropertiesConstants.AWS_BUCKET);
		} catch (Exception e) {
			throw new Exception(e);
		}
	}
	
	private String getFilepath() throws Exception {
		try {
			return ServiceProperties.getMessage(ServicePropertiesConstants.AWS_FILEPATH);
		} catch (Exception e) {
			throw new Exception(e);
		}
	}		
}