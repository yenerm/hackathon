package com.filesquare.service.properties;

import java.util.MissingResourceException;
import java.util.ResourceBundle;
import java.util.StringTokenizer;

public class ServiceProperties {

	private static final String BUNDLE_NAME = "com.filesquare.service.properties.service";

	private static  ResourceBundle RESOURCE_BUNDLE = null;
	
	private static ResourceBundle getInstance(){
		if (RESOURCE_BUNDLE == null) {
			synchronized (BUNDLE_NAME) {
				try {
					RESOURCE_BUNDLE =ResourceBundle.getBundle(BUNDLE_NAME);
				} catch (MissingResourceException e) {
					
				}
			}
		}
		return RESOURCE_BUNDLE;
	}
	

	private ServiceProperties() {
	}

	public static String getMessage(String key) {
		try {
			return getInstance().getString(key);
		} catch (MissingResourceException e) {
			return '!' + key + '!';
		}
	}
	
	public static String getMessage(String key, Object[] objectArray){
		
		String resource = "";

		try {
			resource = getInstance().getString(key);
		
			for(int i = 0; i < objectArray.length ; i++){
				
				resource = replaceAllWords(resource, "{" + i + "}", (String) objectArray[i]);
			}
			
		} catch (MissingResourceException e) {
			resource = '!' + key + '!';
		}

		
		return resource;
	}
	
	static String replaceAllWords(String original, String find, String replacement) {
	    StringBuilder result = new StringBuilder(original.length());
	    String delimiters = "+-*/(),. ";
	    StringTokenizer st = new StringTokenizer(original, delimiters, true);
	    while (st.hasMoreTokens()) {
	        String w = st.nextToken();
	        if (w.equals(find)) {
	            result.append(replacement);
	        } else {
	            result.append(w);
	        }
	    }
	    return result.toString();
	}

}
