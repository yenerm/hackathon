package com.filesquare.service.test;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.net.URL;
import java.util.UUID;

import javax.imageio.ImageIO;

import junit.framework.Assert;

import com.amazonaws.services.s3.model.ObjectMetadata;
import com.filesquare.service.dto.AwsFileDTO;
import com.filesquare.service.test.base.ServiceTest;

public class AmazonS3ServiceTest extends ServiceTest {
	
	@org.junit.Test
	public void testInputStreamFileUpload() throws Exception {
		try {
			URL pageURL = new URL("http://www.fenci.gen.tr/Resimler/ustmenu/test.png");

			BufferedImage originalImage = ImageIO.read(pageURL);
			ByteArrayOutputStream os = new ByteArrayOutputStream();
			ImageIO.write(originalImage, "jpg", os);
			InputStream input = new ByteArrayInputStream(os.toByteArray());
			Long contentLength = new Integer(os.size()).longValue();
			ObjectMetadata metadata = new ObjectMetadata();
			metadata.setContentType("image/jpeg");
			metadata.setContentLength(contentLength);
			
			String key = UUID.randomUUID().toString() + ".jpg";
			
			AwsFileDTO awsFileDTO = new AwsFileDTO(key, input, "image/jpeg", contentLength);
			String file = amazonService.uploadFile(awsFileDTO);
			
			Assert.assertNotNull(file);
			
		} catch (Throwable e) {
			throw new Exception(e);
		}
	}
}