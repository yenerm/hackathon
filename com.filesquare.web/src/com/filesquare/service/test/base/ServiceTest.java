package com.filesquare.service.test.base;

import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.filesquare.service.AmazonService;
import com.mongodb.util.TestCase;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:com/filesquare/service/config/filesquare-service.xml")
public class ServiceTest extends TestCase {

	@Autowired
	@Qualifier(value="amazonServiceImpl")
	protected AmazonService amazonService;
}