package com.filesquare.service.constant;

public class ServicePropertiesConstants {

	public static final String AWS_CREDENTIALS_PROPERTIES = "AwsCredentials.properties";
	public static final String AWS_ACCESS_KEY = "aws.access.key";
	public static final String AWS_SECRET_KEY = "aws.secret.key";
	public static final String AWS_ENDPOINT = "aws.endpoint";
	public static final String AWS_FILEPATH = "aws.filepath";
	public static final String AWS_BUCKET = "aws.bucket";
}
